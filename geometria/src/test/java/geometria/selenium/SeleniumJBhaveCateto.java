package geometria.selenium;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumJBhaveCateto {

	@Test
	public void testeChrome() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\100940745\\git\\geometria-2019-1\\geometria\\lib\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("C:\\Users\\100940745\\git\\geometria-2019-1\\geometria\\src\\main\\java\\webapp\\triangulo.html");

		SelecionarCalculoCateto(driver);
		informarCateto1(driver);
		InformarHipotenusa(driver);
		solicitarCalculo(driver);
		CalularCateto2(driver);

	}

	@Then("Ent�o o cateto2 calculado ser�")
	private void CalularCateto2(WebDriver driver) {
		WebElement elementTotal = driver.findElement(By.id("valorTotal"));

		Assert.assertNotEquals(elementTotal, 8);

	}

	@When("solicito que o c�lculo seja realizado ")
	private void solicitarCalculo(WebDriver driver) {
		WebElement elementButton = driver.findElement(By.id("calcularBtn"));
		elementButton.click();
	}

	@When("seleciono o tipo de c�lculo Cateto")
	private void SelecionarCalculoCateto(WebDriver driver) {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Cateto");
	}

	@When("informo 10 para $hipotenusa")
	private void InformarHipotenusa(WebDriver driver) {
		WebElement elementValor = driver.findElement(By.id("hipotenusa"));
		elementValor.sendKeys("10");
	}

	@When("informo 6 para $cateto1")

	private void informarCateto1(WebDriver driver) {
		WebElement elementQtd = driver.findElement(By.id("cateto1"));
		elementQtd.sendKeys("6");
	}

}
