package geometria.selenium;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumJBhaveHipotenusa {

	@Test
	public void testeChrome() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\100940745\\git\\geometria-2019-1\\geometria\\lib\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("C:\\Users\\100940745\\git\\geometria-2019-1\\geometria\\src\\main\\java\\webapp\\triangulo.html");

		SelecionarCalculoHipotenusa(driver);
		informarCateto1(driver);
		InformarCateto2(driver);
		solicitarCalculo(driver);
		CalularHipotenusa(driver);

	}

	@Then("Ent�o a hipotenusa calculada ser�")
	private void CalularHipotenusa(WebDriver driver) {
		WebElement elementTotal = driver.findElement(By.id("valorTotal"));

		Assert.assertNotEquals(elementTotal, 5);

	}

	@When("solicito que o c�lculo seja realizado ")
	private void solicitarCalculo(WebDriver driver) {
		WebElement elementButton = driver.findElement(By.id("calcularBtn"));
		elementButton.click();
	}

	@When("seleciono o tipo de c�lculo Hipotenusa")
	private void SelecionarCalculoHipotenusa(WebDriver driver) {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Hipotenusa");
	}

	@When("informo 4 para $cateto2")
	private void InformarCateto2(WebDriver driver) {
		WebElement elementValor = driver.findElement(By.id("cateto2"));
		elementValor.sendKeys("4");
	}

	@When("informo 3 para $cateto1")

	private void informarCateto1(WebDriver driver) {
		WebElement elementQtd = driver.findElement(By.id("cateto1"));
		elementQtd.sendKeys("3");
	}

}
